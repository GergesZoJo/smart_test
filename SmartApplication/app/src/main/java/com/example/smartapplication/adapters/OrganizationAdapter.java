package com.example.smartapplication.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smartapplication.R;
import com.example.smartapplication.activities.ListOrganizationsActivity;
import com.example.smartapplication.models.Organization;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrganizationAdapter extends RecyclerView.Adapter<OrganizationAdapter.ViewHolder> {
    Activity activity;
    ArrayList<Organization> organizations;
    Organization organization = new Organization();

    public OrganizationAdapter(Activity activity, ArrayList<Organization> organizations) {
        this.activity = activity;
        this.organizations = organizations;
    }


    @NonNull
    @Override
    public OrganizationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.linearlayout_item_organization, viewGroup, false);
        return new OrganizationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrganizationAdapter.ViewHolder holder, int i) {
        Organization organization = organizations.get(i);
        holder.lblOrganiztionName.setText(organization.getName());
        Log.i("_test", organization.getName());
        holder.encriptionKey.setText( organization.getEncriptionCode() );
        OnClick click = new OnClick();
        click.setRawInfo(organization.getRawDetail());
        holder.lblOrganiztionName.setOnClickListener(click);


    }

    @Override
    public int getItemCount() {
        return organizations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView lblOrganiztionName, encriptionKey;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            lblOrganiztionName  = itemView.findViewById(R.id.textViewLablelNameOrganizationDialog);
            encriptionKey       = itemView.findViewById(R.id.textViewEncriptionCode);
        }
    }

    class OnClick implements View.OnClickListener {
        String rawInfo;

        public String getRawInfo() {
            return rawInfo;
        }

        public void setRawInfo(String rawInfo) {
            this.rawInfo = rawInfo;
        }

        private ArrayList<TextView> getTextViews(ViewGroup root){
            ArrayList<TextView> views=new ArrayList <>();
            for(int i=0;i<root.getChildCount();i++){
                View v=root.getChildAt(i);
                if(v instanceof TextView){
                    Log.i("_test_id_actual", ""+((TextView) v).getId());
                    Log.i("_test_id_actual", ""+R.id.textViewEncriptionCode);
                    if( ((TextView) v).getId() == R.id.textViewEncriptionCode ){
                        Log.i("_test_textView_", "Igual");
                        Log.i("_test_textView_", ((TextView) v).getText().toString());
                        views.add((TextView) v);
                    } else {
                        Log.i( "_test_id diferente", v.getContext().getResources().getResourceEntryName(((TextView) v).getId()).toString());
                    }
                }else if(v instanceof ViewGroup){
                    views.addAll(getTextViews((ViewGroup)v));
                }
            }
            return views;
        }

        @Override
        public void onClick(View v) {
            TextView textView = null;
            final ArrayList<TextView> arrayListTextView = new ArrayList<TextView>();
            ViewGroup row = (ViewGroup) v.getParent();
            arrayListTextView.addAll( getTextViews((ViewGroup)row) );
            Log.i("_test_", arrayListTextView.toString());
            for (TextView s : arrayListTextView) {
                Log.i("_test_textview_", s.getText().toString());
                Log.i("_test_textview_", ""+ v.getContext().getResources().getResourceEntryName(s.getId()));
            }
            Log.i("_test_arrayList_", (arrayListTextView.toArray()).toString());
            Log.i("_test_arrayList_", arrayListTextView.get(0).getText().toString());

            switch (v.getId()){
                case R.id.textViewLablelNameOrganizationDialog:

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    // Get the layout inflater
                    LayoutInflater inflater = activity.getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.dialog_encriptioncode, null);
                    final EditText editTextEncriptionCodeDialog = dialogView.findViewById( R.id.editTextEncriptioCodeDialog );

                    try {
                        JSONObject j = new JSONObject(getRawInfo());

                        organization.setId(j.getInt("id"));
                        organization.setName(j.getString("name"));
                        organization.setAddress(j.getString("address"));
                        organization.setPhone(j.getString("phone"));
                        organization.setExternal_id(j.getString("id_externo"));
                        organization.setEncriptionCode(j.getString("encriptionCode"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.i("_test_organization_", organization.getName());

                    // Inflate and set the layout for the dialog
                    // Pass null as the parent view because its going in the dialog layout
                    builder.setView(dialogView)
                            .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    Log.i("_test", editTextEncriptionCodeDialog.getText().toString());
                                    Log.i("_test_", arrayListTextView.get(0).getText().toString());

                                    if( organization.getEncriptionCode().equals(editTextEncriptionCodeDialog.getText().toString().trim() ) ) {
                                        dialog.dismiss();

                                        AlertDialog.Builder builder2 = new AlertDialog.Builder(activity);
                                        // Get the layout inflater
                                        LayoutInflater inflater2 = activity.getLayoutInflater();
                                        View dialogView2 = inflater2.inflate(R.layout.dialog_organization, null);
                                        final TextView textViewNameDialog = dialogView2.findViewById( R.id.textViewNameOrganizationDialogO );
                                        final TextView textViewAddressDialog = dialogView2.findViewById( R.id.textViewAddressOrganizationDialogO );
                                        final TextView textViewPhoneDialog = dialogView2.findViewById( R.id.textViewPhoneOrganizationDialogO );
                                        final TextView textViewIdExternalDialog = dialogView2.findViewById( R.id.textViewIdExternoOrganizationDialogO );
                                        final TextView textViewEncriptionCodeDialog = dialogView2.findViewById( R.id.textViewEncriptionCodeOrganizationDialogO );

                                        textViewNameDialog.setText(organization.getName());
                                        textViewAddressDialog.setText(organization.getAddress());
                                        textViewPhoneDialog.setText(organization.getPhone());
                                        textViewIdExternalDialog.setText(organization.getExternal_id());
                                        textViewEncriptionCodeDialog.setText(organization.getEncriptionCode());
                                        // Inflate and set the layout for the dialog
                                        // Pass null as the parent view because its going in the dialog layout
                                        builder2.setView(dialogView2)
                                                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog2, int id) {
                                                        dialog2.dismiss();
                                                    }
                                                })
                                                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog2, int id) {
                                                        dialog2.cancel();
                                                    }
                                                });

                                        builder2.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog2, int which) {
                                                dialog2.dismiss();
                                            }
                                        });
                                        final AlertDialog alertDialog2=builder2.create();
                                        alertDialog2.show();
                                    } else {
                                        Toast.makeText(activity, R.string.there_is_an_error_with_the_code, Toast.LENGTH_LONG).show();
                                        //editTextEncriptionCodeDialog.setError(dialogView.getContext().getResources().getString(R.string.there_is_an_error_with_the_code));
                                    }
                                }
                            })
                            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog alertDialog=builder.create();
                    alertDialog.show();

//                Intent intent = new Intent(activity, DetailOrganizationActivity.class);
//                    intent.putExtra("json", getRawInfo());
//                    activity.startActivity(intent);
//                    break;
            }
        }
    }
}

package com.example.smartapplication.constants;

public class APIConstants {
    public static final String URL = "https://arthrex-ecx.com/";

    public static final String SUCCESS_MESSAGE  = "success";

    public static final String URL_LOGIN        = APIConstants.URL + "api/auth/login";
    public static final String URL_SIGNUP       = APIConstants.URL + "api/auth/signup";
    public static final String URL_LOGOUT       = APIConstants.URL + "api/auth/logout";

    public static final String URL_GET_ORGANIZATIONS = APIConstants.URL + "api/auth/get_organizations";
    public static final String URL_SAVE_ORGANIZATION = APIConstants.URL + "api/auth/save_organization";
}

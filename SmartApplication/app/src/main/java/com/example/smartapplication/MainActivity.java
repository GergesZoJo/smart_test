package com.example.smartapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.smartapplication.activities.MainOptionsActivity;
import com.example.smartapplication.webservices.UserWebService;

import com.example.smartapplication.activities.SignUpActivity;

public class MainActivity extends InitialClass implements View.OnClickListener {

    EditText editTextEmail, editTextPassword;
    Button buttonIniciarSesion;
    ProgressBar progressBarLogin;
    UserWebService userWebService;
    Intent intent;

    @Override
    protected void init() {
        editTextEmail    = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonIniciarSesion   = findViewById(R.id.buttonIniciarSesion);
        progressBarLogin      = findViewById(R.id.progressBarLogin);
        userInformation.saveToken("");
        userInformation.saveUserInformation("");
        userWebService  = new UserWebService(getActivity());
        buttonIniciarSesion.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonIniciarSesion:
                new OnLogin().execute();
                break;
            case R.id.buttonRegistrarUsuario:
                intent = new Intent(getActivity(), SignUpActivity.class);
                startActivity(intent);
                break;
        }
    }

    private class OnLogin extends AsyncTask<Void, Void, Void> {
        boolean loggedIn = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBarLogin.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBarLogin.setVisibility(View.GONE);

            if ( loggedIn ) {
                intent = new Intent(getActivity(), MainOptionsActivity.class);
                startActivity(intent);
            }else{
                Toast.makeText(getActivity(), userWebService.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            loggedIn = userWebService.login(extractText(editTextEmail), extractText(editTextPassword));
            return null;
        }
    }
}

package com.example.smartapplication.shared;

import android.app.Activity;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.smartapplication.models.User;

public class UserInformation extends InitialShared {
    private static final String USER_INFO = "userInfo";
    private static final String TOKEN = "access_token";

    public UserInformation(Activity activity) {
        super(activity, "UserInformation");
    }

    public void saveUserInformation(String user) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(USER_INFO, user);
        editor.commit();
    }

    public void saveToken(String access_token) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(TOKEN, "Bearer " + access_token);
        editor.commit();
    }

    public String getToken(){
        return getSharedPreferences().getString(TOKEN, "");
    }

    public User getUser(){
        User user = new User();
        try {
            JSONObject json = new JSONObject(getSharedPreferences().getString(USER_INFO,""));
            user.setId(json.getInt("id"));
            user.setName(json.getString("name"));
            user.setEmail(json.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }
}

package com.example.smartapplication.webservices;

import android.app.Activity;
import android.util.Log;

import com.example.smartapplication.constants.APIConstants;
import com.example.smartapplication.models.Organization;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OrganizationWebService extends InitialWebService {
    public OrganizationWebService(Activity activity) {
        super(activity);
        TAG = "OrganizationWebservice_test";
    }

    public ArrayList<Organization> getOrganizations(){
        ArrayList<Organization> organizations = new ArrayList<>();

        Request newReq = new Request.Builder()
                .url(APIConstants.URL_GET_ORGANIZATIONS)
                .header("Content-Type", "application/json")
                .header("X-Requested-With", "XMLHttpRequest")
                .header("Authorization", userInformation.getToken())
                .build();
        try {
            Response response = httpClient.newCall(newReq).execute();
            String jsonReponse = response.body().string();
            Log.i(TAG, "json_response:" + jsonReponse);
            JSONObject json = new JSONObject(jsonReponse);
            this.setErrorMessage("");
            if(json.has("status")) {
                String serverStatus = json.getString("status");
                if (serverStatus.equals(APIConstants.SUCCESS_MESSAGE)) {
                    JSONArray jsonArray = json.getJSONArray("organizations");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Organization organization = new Organization();
                        JSONObject j = jsonArray.getJSONObject(i);
                        organization.setId(j.getInt("id"));
                        organization.setName(j.getString("name"));
                        organization.setAddress(j.getString("address"));
                        organization.setPhone(j.getString("phone"));
                        organization.setExternal_id(j.getString("id_externo"));
                        organization.setEncriptionCode(j.getString("encriptionCode"));
                        organization.setRawDetail(jsonArray.getString(i));
                        Log.i(TAG, j.toString());
                        organizations.add(organization);
                    }
                } else {
                    setErrorMessage(json.getString("message"));
                }
            } else {
                setErrorMessage(json.getString("message"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return organizations;
    }

    public boolean saveOrganization(Organization organization) {
        boolean saved = false;
        RequestBody body = new FormBody.Builder()
                .add( "name", organization.getName())
                .add("address", organization.getAddress())
                .add("encriptionCode", organization.getEncriptionCode())
                .add("phone", organization.getPhone())
                .build();
        Request newReq = new Request.Builder()
                .url(APIConstants.URL_SAVE_ORGANIZATION)
                .header("Content-Type", "application/json")
                .header("X-Requested-With", "XMLHttpRequest")
                .header("Authorization", userInformation.getToken())
                .post(body)
                .build();
        try {
            Response response = httpClient.newCall(newReq).execute();
            String jsonReponse = response.body().string();
            Log.i(TAG, "json_response:" + jsonReponse);
            JSONObject json = new JSONObject(jsonReponse);
            this.setErrorMessage("");
            if(json.has("status")) {
                String serverStatus = json.getString("status");
                if (serverStatus.equals(APIConstants.SUCCESS_MESSAGE)) {
                    saved = true;
                } else {
                    setErrorMessage(json.getString("message"));
                }
            } else {
                setErrorMessage(json.getString("message"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return saved;
    }
}

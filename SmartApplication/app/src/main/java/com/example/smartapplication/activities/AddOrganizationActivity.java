package com.example.smartapplication.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;

import com.example.smartapplication.InitialClass;
import com.example.smartapplication.R;

import com.example.smartapplication.models.Organization;
import com.example.smartapplication.webservices.OrganizationWebService;

public class AddOrganizationActivity extends InitialClass {

    EditText editTextName, editTextAddress, editTextPhone, editTextEncriptionCode;
    Button buttonSaveOrganization;
    ProgressBar progressBarAddOrganization;
    OrganizationWebService organizationWebService;
    Organization organization = new Organization();

    @Override
    protected void init() {
        TAG = "AddOrganization";
        //setContentView(R.layout.activity_register_organization);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBarAddOrganization              = findViewById(R.id.progressBarAddOrganization);
        editTextName             = findViewById(R.id.editTextName);
        editTextAddress          = findViewById(R.id.editTextAddress);
        editTextPhone            = findViewById(R.id.editTextPhone);
        editTextEncriptionCode   = findViewById(R.id.editTextEncriptionCode);
        buttonSaveOrganization   = findViewById(R.id.buttonSaveOrganization);
        organizationWebService  = new OrganizationWebService(getActivity());
        buttonSaveOrganization.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_organization);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        init();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSaveOrganization:
                new OnRegisterOrganization().execute();
                break;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    class OnRegisterOrganization extends AsyncTask<Void, Void, Void> {
        boolean saved = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBarAddOrganization.setVisibility(View.VISIBLE);

            organization.setName(extractText(editTextName));
            organization.setAddress(extractText(editTextAddress));
            organization.setEncriptionCode(extractText(editTextEncriptionCode));
            organization.setPhone(extractText(editTextPhone));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBarAddOrganization.setVisibility(View.GONE);
            if (saved){
                Toast.makeText(getActivity(), R.string.saved, Toast.LENGTH_LONG).show();
                getActivity().onBackPressed();
            }else{
                Toast.makeText(getActivity(), organizationWebService.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            saved = organizationWebService.saveOrganization(organization);
            return null;
        }
    }
}

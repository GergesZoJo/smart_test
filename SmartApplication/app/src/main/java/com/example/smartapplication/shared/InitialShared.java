package com.example.smartapplication.shared;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class InitialShared {
    private Activity activity;
    private String MY_SHARED;

    public InitialShared(Activity activity, String sharedName){
        this.activity = activity;
        this.MY_SHARED = sharedName;
    }

    protected SharedPreferences getSharedPreferences(){
        SharedPreferences sharedpreferences;
        try {
            sharedpreferences = activity.getSharedPreferences(MY_SHARED, Context.MODE_PRIVATE);
        }catch (NullPointerException ex){
            sharedpreferences = activity.getSharedPreferences(MY_SHARED, Context.MODE_PRIVATE);
        }
        return  sharedpreferences;
    }
}

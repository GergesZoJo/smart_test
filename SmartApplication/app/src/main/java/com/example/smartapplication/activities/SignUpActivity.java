package com.example.smartapplication.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;

import com.example.smartapplication.InitialClass;
import com.example.smartapplication.R;
import com.example.smartapplication.models.User;
import com.example.smartapplication.webservices.UserWebService;

public class SignUpActivity extends InitialClass implements View.OnClickListener {

    EditText editTextName, editTextEmail, editTextPassword, editTextPasswordConfirm;
    ProgressBar progressBarSignUp;
    UserWebService userWebService;
    User user = new User();
    Intent intent;

    @Override
    protected void init() {
        editTextName            = findViewById(R.id.editTextName);
        editTextEmail           = findViewById(R.id.editTextEmail);
        editTextPassword        = findViewById(R.id.editTextPassword);
        editTextPasswordConfirm = findViewById(R.id.editTextPasswordConfirm);
        progressBarSignUp       = findViewById(R.id.progressBarSignUp);
        userWebService          = new UserWebService(getActivity());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSaveUser:
                new OnSignUp().execute();
                break;
        }
    }

    class OnSignUp extends AsyncTask<Void, Void, Void> {

        boolean savedInfo = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBarSignUp.setVisibility(View.VISIBLE);

            user.setName(extractText(editTextName));
            user.setEmail(extractText(editTextEmail));
            user.setPassword(extractText(editTextPassword));
            user.setConfirm(extractText(editTextPasswordConfirm));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressBarSignUp.setVisibility(View.GONE);

            if (savedInfo){
                intent = new Intent(getActivity(), MainOptionsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), userWebService.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            savedInfo = userWebService.register(user);
            return null;
        }
    }
}

package com.example.smartapplication.webservices;

import android.app.Activity;
import android.util.Log;

import com.example.smartapplication.constants.APIConstants;
import com.example.smartapplication.models.User;
import com.example.smartapplication.shared.UserInformation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserWebService extends InitialWebService {
    UserInformation userInformation;

    public UserWebService(Activity activity) {
        super(activity);
        userInformation = new UserInformation(activity);
        TAG = "UserWebservice_test";
    }

    public boolean login(String email, String password){
        boolean loggedIn = false;
        RequestBody body = new FormBody.Builder()
                .add( "email", email)
                .add("password", password)
                .build();


        Request newReq = new Request.Builder()
                .url(APIConstants.URL_LOGIN)
                .header("Content-Type", "application/json")
                .header("X-Requested-With", "XMLHttpRequest")
                .post( body )
                .build();
        try {
            Response response = httpClient.newCall(newReq).execute();
            String jsonReponse = response.body().string();
            Log.i(TAG, "json_response:" + jsonReponse);
            JSONObject json = new JSONObject(jsonReponse);
            this.setErrorMessage("");
            if(json.has("status")) {
                String serverStatus = json.getString("status");
                if (serverStatus.equals(APIConstants.SUCCESS_MESSAGE)) {
                    userInformation.saveToken(json.getString("access_token"));
                    userInformation.saveUserInformation(json.getString("user"));
                    loggedIn = true;
                } else {
                    setErrorMessage(json.getString("message"));
                }
            } else {
                setErrorMessage(json.getString("message"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return loggedIn;
    }

    public boolean register(User user) {
        boolean saved = false;
        RequestBody body = new FormBody.Builder()
                .add( "name", user.getName())
                .add("email", user.getEmail())
                .add("password", user.getPassword())
                .add("password_confirmation", user.getConfirm())
                .build();
        Request newReq = new Request.Builder()
                .url(APIConstants.URL_SIGNUP)
                .header("Content-Type", "application/json")
                .header("X-Requested-With", "XMLHttpRequest")
                .post(body)
                .build();
        try {
            Response response = httpClient.newCall(newReq).execute();
            String jsonReponse = response.body().string();
            Log.i(TAG, "json_response:" + jsonReponse);
            JSONObject json = new JSONObject(jsonReponse);
            this.setErrorMessage("");
            if(json.has("status")) {
                String serverStatus = json.getString("status");
                if (serverStatus.equals(APIConstants.SUCCESS_MESSAGE)) {
                    login(user.getEmail(), user.getPassword());
                    saved = true;
                } else {
                    setErrorMessage(json.getString("message"));
                }
            } else {
                setErrorMessage(json.getString("message"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return saved;
    }
}

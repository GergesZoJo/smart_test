package com.example.smartapplication.webservices;

import android.app.Activity;

import com.example.smartapplication.shared.UserInformation;

import okhttp3.OkHttpClient;


public class InitialWebService {
    protected Activity activity;
    protected String errorMessage = "";
    protected String TAG = "initialWebService";
    protected OkHttpClient httpClient = new OkHttpClient();
    protected UserInformation userInformation;

    public InitialWebService(Activity activity) {
        this.activity = activity;
        userInformation = new UserInformation(activity);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getTAG() {
        return TAG;
    }
    public void setTAG(String TAG){
        this.TAG = TAG;
    }
}

package com.example.smartapplication.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.smartapplication.InitialClass;
import com.example.smartapplication.R;
import com.example.smartapplication.adapters.OrganizationAdapter;
import com.example.smartapplication.models.Organization;
import com.example.smartapplication.webservices.OrganizationWebService;

import java.util.ArrayList;

public class ListOrganizationsActivity extends InitialClass {

    RecyclerView recyclerView;
    ProgressBar progressBarOrganization;
    OrganizationWebService organizationWebService;
    ArrayList<Organization> organizations;

    @Override
    protected void init() {
        recyclerView            = findViewById(R.id.recyclerViewOrganization);
        progressBarOrganization = findViewById(R.id.progressBar);
        organizationWebService  = new OrganizationWebService(getActivity());
        new OnLoad().execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_organizations);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        init();
    }

    @Override
    public void onClick(View v) {

    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private class OnLoad extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBarOrganization.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressBarOrganization.setVisibility(View.GONE);

            if (!organizationWebService.getErrorMessage().equals("")){
                Toast.makeText(getActivity(), organizationWebService.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
            if (organizations.size() == 0){
                Toast.makeText(getActivity(), R.string.there_are_no_organizations, Toast.LENGTH_LONG).show();
            } else {
                Log.i("adapter ", "agregar adapter");
                OrganizationAdapter adapter = new OrganizationAdapter(getActivity(), organizations);
                Log.i(TAG, "Organizations: "+ organizations.size());
                recyclerView.setAdapter(adapter);
                //GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
                //recyclerView.setLayoutManager(layoutManager);


                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(llm);
                adapter.notifyDataSetChanged();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            organizations = organizationWebService.getOrganizations();

            return null;
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Organization;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function saveOrganization(Request $request)
    {
        $request->validate([
            'name'              => 'required|string',
            'address'           => 'required|string',
            'phone'             => 'required|string',
            'encriptionCode'    => 'required|string|size:4',
        ]);
        
        $totalRegistros = Organization::count();
        
        $organization = new Organization([
            'name'              => $request->name,
            'address'             => $request->address,
            'phone'             => $request->phone,
            'encriptionCode'    => $request->encriptionCode,
            'id_externo'        => "NE" . substr($request->name, 0, 4) . substr( $request->phone, -4 ) . 
                                    ( 
                                        ( $totalRegistros == 0 ) ? // Sin Registros
                                            1
                                            :
                                            ( 
                                                ( // Con registros
                                                    ( $totalRegistros % 999 ) > 0 ? 
                                                        ( ( $totalRegistros % 999 ) + 1 ) // Registros no divisibles con 999
                                                        :
                                                        1 // Registros divisibles con 999
                                                )
                                            )
                                    ),
        ]);
        
        $organization->save();
        
        return response()->json([
            "status"    => "success",
            'message'   => 'Successfully created organization!'], 201);
    }

    public function listOrganization(Request $request)
    {
        return response()->json(["status"    => "success", "organizations" => Organization::get()]);
    }
}
